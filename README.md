Supervised Learning Experimental Framework

Here is a brief overview of the subdirectories and files contained within:

# Subdirectories

## cropped_images
Contains cropped versions of the original captured images

## cropped_resized_images
Contains resized versions of the cropped images

## final_images
Contains the images (as PNG files) for each of the four folds used in 4-fold cross validation.

## final_images_rgb
Contains the image rgb data (in txt files) for each image in the 4 folds. Structured the same as the final_images subdirectory.

## src
Contains python scripts to run the SL experiments. Each script has some documentation.
Also houses the result images for Model 1 and Model 2 as seen in the thesis document.

# Files

## quadrants.txt
Contains information about what quadrant of the maze Ms. Pac-Man is in for each of the images in the dataset.