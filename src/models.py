"""Defines the models used in the experiments"""
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.utils import np_utils


def create_model(model_num, num_classes, img_channels, img_rows, img_columns):
    if model_num == 1:
        return lenet_model(num_classes, img_channels, img_rows, img_columns)
    elif model_num == 2:
        return custom_model(num_classes, img_channels, img_rows, img_columns)


def lenet_model(num_classes, img_channels, img_rows, img_columns):
    model = Sequential()
    model.add(Convolution2D(6, 5, 5, border_mode='same', activation='relu', init='he_normal',
                            input_shape=(img_channels, img_rows, img_columns)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Convolution2D(16, 5, 5, activation='relu', init='he_normal'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Convolution2D(120, 5, 5, activation='relu', init='he_normal'))
    model.add(Flatten())
    model.add(Dense(84, activation='relu'))
    #model.add(Dropout(0.25))
    model.add(Dense(num_classes, activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
    model.summary()
    return model


def custom_model(num_classes, img_channels, img_rows, img_columns):
    model = Sequential()
    model.add(Convolution2D(32, 10, 10, border_mode='same', activation='relu', init='he_normal',
                            input_shape=(img_channels, img_rows, img_columns)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Convolution2D(32, 8, 8, activation='relu', init='he_normal'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Convolution2D(64, 5, 5, activation='relu', init='he_normal'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Convolution2D(64, 3, 3, activation='relu', init='he_normal'))
    model.add(Flatten())
    model.add(Dense(200, activation='relu'))
    #model.add(Dropout(0.25))
    model.add(Dense(num_classes, activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
    return model
