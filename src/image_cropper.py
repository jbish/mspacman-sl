#!/usr/bin/python
"""Crops raw images"""
from PIL import Image


def crop_images(num_examples):
    prefixes = ['down', 'left', 'up', 'right']
    for i in range(1, 5):
        for prefix in prefixes:
            for j in range(1, num_examples+1):
                name = '../raw_images/maze{0}/{1}{2}.png'.format(i, prefix, j)
                img = Image.open(name)
                cropped = img.crop((722, 258, 1171, 755))
                cropped.save('../cropped_images/maze{0}-{1}{2}.png'.format(i, prefix, j))
