#!/usr/bin/python
"""Main driver script for automating the generation of the dataset.

As per the code at the bottom:
- Crops images
- Resizes images
- Creates CV folds
- Creates translations
- Generates RGB data for the images"""
import os
import shutil
import image_cropper
import image_resizer
import cv_fold_selection
import create_translations
import image_rgb_converter

num_folds = 4  # number of cross validation folds
num_examples = 16  # number of examples of each action on each maze
examples_per_fold = 4  # number of examples of each (action, maze) pair to be in each fold

resized_image_width, resized_image_height = 80, 80  # dimensions of resized images

num_translations = 5 # number of translations to perform on each image
# divisors for calculating allowed translation size
max_x_divisor, max_y_divisor, min_x_divisor, min_y_divisor = 10, 10, 8, 8


def delete_files():
    # remove cropped and resized images
    cropped_images = os.listdir('../cropped_images')
    cropped_resized_images = os.listdir('../cropped_resized_images')
    for image in cropped_images:
        os.remove(os.path.join('../cropped_images', image))
    for image in cropped_resized_images:
        os.remove(os.path.join('../cropped_resized_images', image))
    # Remove all fold data
    base_dirs = ['../final_images', '../final_images_rgb']
    for d in base_dirs:
        sub_dirs = [name for name in os.listdir(d) if os.path.isdir(os.path.join(d, name))]
        for s in sub_dirs:
            shutil.rmtree(os.path.join(d, s))


delete_files()
image_cropper.crop_images(num_examples)
image_resizer.resize_cropped_images(resized_image_width, resized_image_height)
cv_fold_selection.select_folds(num_folds, examples_per_fold)
create_translations.create_translations(num_translations, max_x_divisor, max_y_divisor, min_x_divisor,
                                        min_y_divisor)
image_rgb_converter.gen_all()
