"""Loads the training and test datasets for CV from the RGB data saved in
the final_images_rgb directory."""
import os
import numpy as np


def load_dataset(num_folds, examples_per_fold, img_rows, img_columns, num_translations):
    (x_train, y_train), (x_train_translated, y_train_translated) = \
        load_train_set(num_folds, examples_per_fold, img_rows, img_columns, num_translations)
    return (x_train, y_train), (x_train_translated, y_train_translated)


def load_train_set(num_folds, examples_per_fold, img_rows, img_columns, num_translations):
    x_train = np.empty((num_folds, examples_per_fold*16, 3, img_rows, img_columns))
    y_train = np.zeros((num_folds, examples_per_fold*16, 4))
    x_train_translated = np.empty((num_translations, num_folds, examples_per_fold*16, 3, img_rows, img_columns))
    y_train_translated = np.zeros((num_translations, num_folds, examples_per_fold*16, 4))

    base_path = '../final_images_rgb'
    for i in range(0, num_folds):
        fold_path = os.path.join(base_path, 'fold{0}'.format(i+1))
        # get data for original images
        original_images = [f for f in os.listdir(fold_path) if os.path.isfile(os.path.join(fold_path, f))]
        # sort original images list
        original_images = sort_images_list(original_images)
        for j in range(0, len(original_images)):
            image = original_images[j]
            array = get_rgb_array(fold_path, image, img_rows, img_columns)
            x_train[i][j] = array
            if 'up' in image:
                y_train[i][j][0] = 1
            elif 'down' in image:
                y_train[i][j][1] = 1
            elif 'left' in image:
                y_train[i][j][2] = 1
            else:
                y_train[i][j][3] = 1
        # get data for translated images
        translation_path = os.path.join(fold_path, 'translations')
        translated_images = os.listdir(translation_path)
        translated_images = sort_images_list(translated_images)
        for j in range(0, len(translated_images)):
            image = translated_images[j]
            array = get_rgb_array(translation_path, image, img_rows, img_columns)
            # get number of the translation
            translation_num = int(image[-5])  # will only work if num_translations < 10
            t = translation_num
            j /= num_translations  # because we are iterating through the transformation arrays in parallel
            x_train_translated[t-1][i][j] = array
            if 'up' in image:
                y_train_translated[t-1][i][j][0] = 1
            elif 'down' in image:
                y_train_translated[t-1][i][j][1] = 1
            elif 'left' in image:
                y_train_translated[t-1][i][j][2] = 1
            else:
                y_train_translated[t-1][i][j][3] = 1
    return (x_train, y_train), (x_train_translated, y_train_translated)


def sort_images_list(images):
    return sorted(images, key=lambda image: image[:-4])  # sort based on filename (without .txt extension)


def get_rgb_array(path, image, img_rows, img_columns):
    result = np.empty((3, img_rows, img_columns))
    red = np.empty(img_rows*img_columns)
    green = np.empty(img_rows*img_columns)
    blue = np.empty(img_rows*img_columns)
    im_path = os.path.join(path, image)
    f = open(im_path, 'r')
    i = 0
    # read into one long array
    for line in f:
        line = line.rstrip('\n')
        line = line.split()
        r, g, b = line[0], line[1], line[2]
        red[i] = r
        green[i] = g
        blue[i] = b
        i += 1
    # reshape to be img_rows x img_columns
    result[0] = np.reshape(red, (img_rows, img_columns))
    result[1] = np.reshape(green, (img_rows, img_columns))
    result[2] = np.reshape(blue, (img_rows, img_columns))
    return result
