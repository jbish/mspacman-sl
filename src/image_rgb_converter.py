#!/usr/bin/python
"""Generates the RGB data for a cropped and resized image"""
import os
from PIL import Image


def gen_rgb(image, path):
    im_path = os.path.join(path, image)
    im = Image.open(im_path)
    rgb_im = im.convert('RGB')
    # get new path (replace 'final_images' in path name with 'final_images_rgb') since dirs have mirrored structure
    new_path = path.replace('final_images', 'final_images_rgb')
    f = open('{0}/{1}{2}'.format(new_path, image[:-3], 'txt'), 'w')  # new file with txt extension
    # top left hand pixel is (0, 0)
    width, height = rgb_im.size
    # fill in row by row
    for i in range(0, height):
        for j in range(0, width):
            r, g, b = rgb_im.getpixel((j, i))  # note the (j, i) index here!
            f.write('{0} {1} {2}\n'.format(str(r), str(g), str(b)))
    f.close()


def gen_all():
    base_path = '../final_images'
    sub_dirs = [name for name in os.listdir(base_path) if os.path.isdir(os.path.join(base_path, name))]
    for s in sub_dirs:
        fold_path = os.path.join(base_path, s)
        original_images = [f for f in os.listdir(fold_path) if os.path.isfile(os.path.join(fold_path, f))]
        for image in original_images:
            gen_rgb(image, fold_path)
        translated_path = os.path.join(fold_path, 'translations')
        translated_images = os.listdir(translated_path)
        for image in translated_images:
            gen_rgb(image, translated_path)
