#!/usr/bin/python
"""Creates translations for an image"""
from __future__ import division
from PIL import Image
import os
import math
import random


def create_translations(num_translations, max_x_divisor, max_y_divisor, min_x_divisor, min_y_divisor):
    # Get dictionary of quadrants
    quadrant_dict = {}
    f = open('../quadrants.txt', 'r')
    for line in f:
        line = line.rstrip('\n')
        line = line.split()
        quadrant_dict[line[0]] = line[1]
    f.close()

    # Generate new images for each image by applying random translations based on the image's quadrant
    base_path = '../final_images'
    fold_dirs = [name for name in os.listdir(base_path) if os.path.isdir(os.path.join(base_path, name))]
    for d in fold_dirs:
        fold_path = os.path.join(base_path, d)
        # make a subdirectory for translations
        translation_path = os.path.join(fold_path, 'translations')
        os.mkdir(translation_path)
        # also one for the rgb data
        rgb_fold_path = fold_path.replace('final_images', 'final_images_rgb')
        os.mkdir(os.path.join(rgb_fold_path, 'translations'))
        # get only the image files for this fold, no dirs!
        images = [f for f in os.listdir(fold_path) if os.path.isfile(os.path.join(fold_path, f))]
        for image in images:
            im_path = os.path.join(fold_path, image)
            im = Image.open(im_path)
            im.convert('RGB')

            # get size of the image and determine the maximum amount of translation that can occur
            width, height = im.size
            # want to allow translation of maximum 1/3 of the image width/height in the appropriate direction
            max_x_shift = int(math.floor(width/max_x_divisor))
            max_y_shift = int(math.floor(height/max_y_divisor))
            # also want to shift some minimum amount
            min_x_shift = int(math.floor(max_x_shift/min_x_divisor))
            min_y_shift = int(math.floor(max_y_shift/min_y_divisor))

            # determine the translations
            quadrant = quadrant_dict[image[:-4]]
            translations = []
            for i in range(0, num_translations):
                translation = None
                if quadrant == '1':
                    # both shifts are positive
                    translation = (random.randint(min_x_shift, max_x_shift), random.randint(min_y_shift, max_y_shift))
                elif quadrant == '2':
                    # x shift is negative, y shift is positive
                    translation = (-random.randint(min_x_shift, max_x_shift), random.randint(min_y_shift, max_y_shift))
                elif quadrant == '3':
                    # x shift is positive, y shift is negative
                    translation = (random.randint(min_x_shift, max_x_shift), -random.randint(min_y_shift, max_y_shift))
                else:
                    # both shifts are negative
                    translation = (-random.randint(min_x_shift, max_x_shift), -random.randint(min_y_shift, max_y_shift))
                translations.append(translation)

            for t in range(0, len(translations)):
                translation = translations[t]
                # determine the point to start shifting from (measured from top left corner)
                start_point = None
                if quadrant == '1':
                    start_point = (0, 0)
                elif quadrant == '2':
                    start_point = (width-1, 0)
                elif quadrant == '3':
                    start_point = (0, height-1)
                else:
                    start_point = (width-1, height-1)

                # fill in the lost area with black pixels
                new_im = im.copy()

                start_x = start_point[0]
                x_shift = translation[0]
                end_x = start_x + x_shift
                increment = 1
                if end_x < start_x:
                    increment = -1
                for i in range(start_x, end_x, increment):
                    for j in range(0, height):
                        new_im.putpixel((i, j), (0, 0, 0))  # put a black pixel

                start_y = start_point[1]
                y_shift = translation[1]
                end_y = start_y + y_shift
                increment = 1
                if end_y < start_y:
                    increment = -1
                for i in range(start_y, end_y, increment):
                    for j in range(0, width):
                        new_im.putpixel((j, i), (0, 0, 0))  # put a black pixel

                # copy content from the original image
                x_increment = 1
                y_increment = 1
                start_x = end_x  # start filling in the original image where we ended filling in black pixels
                end_x = None
                start_y = end_y
                end_y = None
                # determine end_x and end_y
                if quadrant == '1':
                    end_x = width
                    end_y = height
                elif quadrant == '2':
                    end_x = 0-1  # since range() function goes from start to end (non-inclusive), and want to include 0
                    x_increment = -1
                    end_y = height
                elif quadrant == '3':
                    end_x = width
                    end_y = 0-1
                    y_increment = -1
                else:
                    end_x = 0-1
                    x_increment = -1
                    end_y = 0-1
                    y_increment = -1

                for i in range(start_x, end_x, x_increment):
                    for j in range(start_y, end_y, y_increment):
                        # get rgb values from original image
                        r, g, b = im.getpixel((i-x_shift, j-y_shift))
                        new_im.putpixel((i, j), (r, g, b))

                new_im.save(os.path.join(translation_path, '{0}-translation{1}.png'.format(image[:-4], t+1)))
