"""Creates a performance graph for a model"""
import matplotlib.pyplot as plt
import numpy as np


def plot(metrics_dict, num_epochs, num_models, num_iterations):
    epochs = np.asarray(list(range(1, num_epochs+1)))
    for i in range(0, num_models):
        plt.figure(i+1)
        plt.title('Model {0} Performance'.format(i+1))
        plt.xlabel('Number of epochs')
        plt.ylabel('Percentage accuracy')
        train_colours = [(1, 0.7, 0.7), (0.65, 0.9, 0.65), (0.65, 0.85, 1)]
        val_colours = ['red', 'green', 'blue']
        for j in range(0, num_iterations):
            # plot losses
            avg_acc = metrics_dict['model{0}'.format(i+1)]['iter{0}'.format(j+1)]['avg_acc']*100
            avg_val_acc = metrics_dict['model{0}'.format(i+1)]['iter{0}'.format(j+1)]['avg_val_acc']*100

            plt.plot(epochs, avg_acc, color=train_colours[j])
            plt.plot(epochs, avg_val_acc, color=val_colours[j])
        plt.savefig('model{0}-performance.png'.format(i+1))
