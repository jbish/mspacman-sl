#!/usr/bin/python
"""Resizes cropped images"""
import os
import math
from PIL import Image


def resize_cropped_images(new_width, new_height):
    path = '../cropped_images'
    images = os.listdir(path)
    for image in images:
        im_path = os.path.join(path, image)
        im = Image.open(im_path)
        resized_im = im.resize((new_width, new_height), Image.ANTIALIAS)
        resized_im.save(os.path.join('../cropped_resized_images', image))
