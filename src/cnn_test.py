"""Main script to perform training and plot performance of a model."""
from __future__ import print_function
import load_dataset
from keras.callbacks import ModelCheckpoint
import numpy as np
import models
import plotting

batch_size = 16
num_classes = 4
num_epochs = 200

num_folds = 4
examples_per_fold = 4

num_iterations = 3  # number of iterations to perform CV on each model for
num_models = 2  # number of models being trained

num_translations = 5

img_rows, img_columns = 80, 80
img_channels = 3

(x_train, y_train), (x_train_translated, y_train_translated) = \
    load_dataset.load_dataset(num_folds, examples_per_fold, img_rows, img_columns, num_translations)

# convert x data to float32 and normalise
x_train = x_train.astype('float32')
x_train_translated = x_train_translated.astype('float32')
x_train /= 255
x_train_translated /= 255

#lenet = models.lenet_model(num_classes, img_channels, img_rows, img_columns)
#lenet.load_weights('../models/model1/iter1/fold1/weights.09.hdf5')
#img = load_dataset.get_rgb_array('../final_images_rgb/fold1', 'maze1-down9.txt', img_rows, img_columns)
#vals = np.empty((1, img_channels, img_rows, img_columns))
#vals[0] = img
#prediction = lenet.predict_classes(vals)

metrics_dict = {}
for i in range(0, num_models):
    metrics_dict['model{0}'.format(i + 1)] = {}
    for j in range(0, num_iterations):
        value = metrics_dict['model{0}'.format(i + 1)]
        metrics = {}
        metrics['avg_acc'] = np.zeros(num_epochs)
        metrics['avg_val_acc'] = np.zeros(num_epochs)
        metrics['avg_loss'] = np.zeros(num_epochs)
        metrics['avg_val_loss'] = np.zeros(num_epochs)
        value['iter{0}'.format(j + 1)] = metrics

for i in range(0, num_iterations):
    print('Iteration number {0}'.format(i + 1))
    for j in range(0, num_models):
        print('Model number {0}'.format(j + 1))
        # perform k-folds cross validation, with k = num_folds
        for k in range(0, num_folds):
            model = models.create_model(j+1, num_classes, img_channels, img_rows, img_columns)
            print('Fold number {0}'.format(k + 1))
            validation_data = (x_train[k], y_train[k])
            # number of total folds generated including translated data
            train_first_dim = (num_folds - 1) * (num_translations + 1)
            train_second_dim = examples_per_fold * 16
            training_data_x = \
                np.empty((train_first_dim, train_second_dim, img_channels, img_columns, img_rows))
            training_data_y = np.zeros((train_first_dim, train_second_dim, 4))
            index = 0
            for n in range(0, num_folds):
                if n != k:
                    training_data_x[index] = x_train[n]
                    for t in range(0, num_translations):
                        training_data_x[index + (t+1)] = x_train_translated[t][n]
                    training_data_y[index] = y_train[n]
                    for t in range(0, num_translations):
                        training_data_y[index + (t+1)] = y_train_translated[t][n]
                    index += (num_translations+1)
            # reshape training data x and y
            training_data_x = np.reshape(training_data_x,
                                         (train_first_dim * train_second_dim, img_channels, img_columns, img_rows))
            training_data_y = np.reshape(training_data_y, (train_first_dim * train_second_dim, 4))
            save_model_checkpoint = ModelCheckpoint('../models/model{0}/iter{1}/fold{2}'.format(j + 1, i + 1, k + 1) +
                                                    '/weights.{epoch:02d}.hdf5', verbose=0, save_best_only=False)
            history = model.fit(training_data_x, training_data_y, batch_size=batch_size, nb_epoch=num_epochs,
                                validation_data=validation_data, shuffle=True, callbacks=[save_model_checkpoint])
            history_dict = history.history

            # record metrics in data dictionary
            acc = np.asarray(history_dict['acc'])/num_folds
            val_acc = np.asarray(history_dict['val_acc'])/num_folds
            loss = np.asarray(history_dict['loss'])/num_folds
            val_loss = np.asarray(history_dict['val_loss'])/num_folds

            avg_acc = metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_acc']
            avg_val_acc = metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_val_acc']
            avg_loss = metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_loss']
            avg_val_loss = metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_val_loss']

            avg_acc += acc
            metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_acc'] = avg_acc
            avg_val_acc += val_acc
            metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_val_acc'] = avg_val_acc
            avg_loss += loss
            metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_loss'] = avg_loss
            avg_val_loss += val_loss
            metrics_dict['model{0}'.format(j + 1)]['iter{0}'.format(i + 1)]['avg_val_loss'] = avg_val_loss

plotting.plot(metrics_dict, num_epochs, num_models, num_iterations)
