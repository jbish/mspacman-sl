"""Saves an image to a png from an RGB array form"""
from PIL import Image


def save_image(img_array, img_width, img_height, name):
    rgb = []
    for i in range(0, img_width):
        for j in range(0, img_height):
            r = int(255*float(img_array[0][i][j]))
            g = int(255*float(img_array[1][i][j]))
            b = int(255*float(img_array[2][i][j]))
            rgb.append((r, g, b))
    im = Image.new('RGB', (img_width, img_height))
    im.putdata(rgb)
    im.save('{0}.png'.format(name))
