#!/usr/bin/python
"""Selects folds for CV"""
import os
import random
import shutil


def select_folds(num_folds, examples_per_fold):
    # get list of images in cropped resized images folder
    path = '../cropped_resized_images'
    images = os.listdir(path)
    # create a multi-level dictionary of this list which holds image file names hierarchically based on their maze
    # number and action type.
    maze1_actions = {'up': [], 'down': [], 'left': [], 'right': []}
    maze2_actions = {'up': [], 'down': [], 'left': [], 'right': []}
    maze3_actions = {'up': [], 'down': [], 'left': [], 'right': []}
    maze4_actions = {'up': [], 'down': [], 'left': [], 'right': []}
    image_dict = {'maze1': maze1_actions, 'maze2': maze2_actions, 'maze3': maze3_actions, 'maze4': maze4_actions}
    for image in images:
        if 'maze1' in image:
            if 'up' in image:
                # update dictionary entry
                files = image_dict['maze1']['up']
                files.append(image)
                image_dict['maze1']['up'] = files
            elif 'down' in image:
                files = image_dict['maze1']['down']
                files.append(image)
                image_dict['maze1']['down'] = files
            elif 'left' in image:
                files = image_dict['maze1']['left']
                files.append(image)
                image_dict['maze1']['left'] = files
            else:
                files = image_dict['maze1']['right']
                files.append(image)
                image_dict['maze1']['right'] = files
        elif 'maze2' in image:
            if 'up' in image:
                files = image_dict['maze2']['up']
                files.append(image)
                image_dict['maze2']['up'] = files
            elif 'down' in image:
                files = image_dict['maze2']['down']
                files.append(image)
                image_dict['maze2']['down'] = files
            elif 'left' in image:
                files = image_dict['maze2']['left']
                files.append(image)
                image_dict['maze2']['left'] = files
            else:
                files = image_dict['maze2']['right']
                files.append(image)
                image_dict['maze2']['right'] = files
        elif 'maze3' in image:
            if 'up' in image:
                files = image_dict['maze3']['up']
                files.append(image)
                image_dict['maze3']['up'] = files
            elif 'down' in image:
                files = image_dict['maze3']['down']
                files.append(image)
                image_dict['maze3']['down'] = files
            elif 'left' in image:
                files = image_dict['maze3']['left']
                files.append(image)
                image_dict['maze3']['left'] = files
            else:
                files = image_dict['maze3']['right']
                files.append(image)
                image_dict['maze3']['right'] = files
        else:
            if 'up' in image:
                files = image_dict['maze4']['up']
                files.append(image)
                image_dict['maze4']['up'] = files
            elif 'down' in image:
                files = image_dict['maze4']['down']
                files.append(image)
                image_dict['maze4']['down'] = files
            elif 'left' in image:
                files = image_dict['maze4']['left']
                files.append(image)
                image_dict['maze4']['left'] = files
            else:
                files = image_dict['maze4']['right']
                files.append(image)
                image_dict['maze4']['right'] = files

    # now, select num_folds number of folds from this data, making sure that each (action, maze) pair is equally
    # represented in each fold.
    mazes = ['maze1', 'maze2', 'maze3', 'maze4']
    actions = ['up', 'down', 'left', 'right']
    for i in range(0, num_folds):
        # make a directory for this fold
        fold_path = '../final_images/fold{0}'.format(i+1)
        os.mkdir(fold_path)
        # also one for the rgb data
        os.mkdir('../final_images_rgb/fold{0}'.format(i+1))
        for maze in mazes:
            for action in actions:
                # select random images for this (maze, action) pair
                choices = image_dict[maze][action]
                for j in range(0, examples_per_fold):
                    choice = random.choice(choices)
                    choices.remove(choice)
                    # update dictionary value
                    image_dict[maze][action] = choices
                    # copy image file
                    shutil.copy(os.path.join(path, choice), os.path.join(fold_path, choice))
